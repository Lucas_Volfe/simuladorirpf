import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-irpf',
  templateUrl: './irpf.component.html',
  styleUrls: ['./irpf.component.scss']
})
export class IrpfComponent implements OnInit {


  public registerForm: FormGroup;

  constructor(
      private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      rendimentoTributavel: [0, [Validators.required]],
      previdenciaOficial: [0, [Validators.required]],
      dependente: [0, [Validators.required]],
      dependenteQuantidade: [0, [Validators.required]],
      alimentandos: [0, [Validators.required]],
      alimentandosQuantidade: [0, [Validators.required]],
      despesasComInstituicoes: [0, [Validators.required]],
      despesasMedicas: [0, [Validators.required]],
      pensaoAlimenticia: [0, [Validators.required]],
      outrasDeducoes: [0, [Validators.required]],
      totaisDasDeducoes: [0, [Validators.required]],
      calculoImposto: [0, [Validators.required]],
      baseDeCalculo: [0, [Validators.required]],
      valorDoImposto: [0, [Validators.required]],
      parcelaADeduzirDoImposto: [0, [Validators.required]],
      deducoesDeIncentivo: [0, [Validators.required]],
      deducaoDoPronasPcd: [0, [Validators.required]],
      deducaoDoPronon: [0, [Validators.required]],
      impostoDevidoUm: [0, [Validators.required]],
      contribuicoesPatronalPrevSocial: [0, [Validators.required]],
      impostoDevidoDois: [0, [Validators.required]],
      aliquotaEfetiva: [0, [Validators.required]],
      irrfAntecipado: [0, [Validators.required]],
      irAPagarOuRecuperar: [0, [Validators.required]],
    });


  }

  calc() {

    this.registerForm.controls.dependente.setValue(
        2275.08 * this.registerForm.controls.dependenteQuantidade.value
    );

    this.registerForm.controls.alimentandos.setValue(
        2275.08 * this.registerForm.controls.alimentandosQuantidade.value
    );

    this.registerForm.controls.totaisDasDeducoes.setValue(
        this.registerForm.controls.previdenciaOficial.value +
        this.registerForm.controls.dependente.value +
        this.registerForm.controls.alimentandos.value +
        this.registerForm.controls.despesasComInstituicoes.value +
        this.registerForm.controls.despesasMedicas.value +
        this.registerForm.controls.pensaoAlimenticia.value +
        this.registerForm.controls.outrasDeducoes.value
    );

    this.registerForm.controls.baseDeCalculo.setValue(
        this.registerForm.controls.rendimentoTributavel.value -
        this.registerForm.controls.totaisDasDeducoes.value
    );

    this.registerForm.controls.calculoImposto.setValue(
        this.registerForm.controls.valorDoImposto.value -
        this.registerForm.controls.parcelaADeduzirDoImposto.value
    );

    this.registerForm.controls.valorDoImposto.setValue(
        this.registerForm.controls.baseDeCalculo.value *
        this.registerForm.controls.aliquotaEfetiva.value / 100
    );

    this.registerForm.controls.impostoDevidoUm.setValue(
        this.registerForm.controls.calculoImposto.value -
        this.registerForm.controls.deducoesDeIncentivo.value -
        this.registerForm.controls.deducaoDoPronasPcd.value -
        this.registerForm.controls.deducaoDoPronon.value
    );

    this.registerForm.controls.impostoDevidoDois.setValue(
        this.registerForm.controls.impostoDevidoUm.value -
        this.registerForm.controls.contribuicoesPatronalPrevSocial.value
    );

    this.registerForm.controls.irAPagarOuRecuperar.setValue(
        this.registerForm.controls.impostoDevidoDois.value -
        this.registerForm.controls.irrfAntecipado.value
    );

    var baseCalc = this.registerForm.controls.baseDeCalculo.value;
    if(baseCalc <= 22847.76){
      this.registerForm.controls.aliquotaEfetiva.setValue(0);
      this.registerForm.controls.parcelaADeduzirDoImposto.setValue(0);
    }
    else if(baseCalc <= 33919.8){
      this.registerForm.controls.aliquotaEfetiva.setValue(7.5);
      this.registerForm.controls.parcelaADeduzirDoImposto.setValue(1713.6);
    }
    else if(baseCalc <= 45012.6){
      this.registerForm.controls.aliquotaEfetiva.setValue(15);
      this.registerForm.controls.parcelaADeduzirDoImposto.setValue(4257.6);
    }
    else if(baseCalc <= 55976.16){
      this.registerForm.controls.aliquotaEfetiva.setValue(27.5);
      this.registerForm.controls.parcelaADeduzirDoImposto.setValue(7633.56);
    }

  }
}
