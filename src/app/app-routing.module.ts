import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {IrpfComponent} from "./irpf/irpf.component";


const routes: Routes = [
  {
    path: '',
    component: IrpfComponent,
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
